const products = [{
		    id: 123,
		    category: 'Newspaper',
		    country: 'India',
		    tax: 18,
		    price: 250,
		    quantity: 3
		}, {
		    id: 124,
		    category: 'Newspaper',
		    country: 'Dubai',
		    tax: 5,
		    price: 300,
		    quantity: 7
		}, {
		    id: 125,
		    category: 'Newspaper',
		    country: 'London',
		    tax: 22,
		    price: 400,
		    quantity: 10
		}, {
		    id: 126,
		    category: 'Newspaper',
		    country: 'France',
		    tax: 13,
		    price: 150,
		    quantity: 24
		}];

function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
}

function generateTable(table, data) {
  for (let element of data) {
    let row = table.insertRow();
    for (key in element) {
      let cell = row.insertCell();
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
  }
}
var result_1 = products.forEach(function(item){
    var finalPrice = (item.price + (item.tax/100)*item.price)*item.quantity;
         item.finalPrice = finalPrice;                        
    });
	var result = products.map(person => ({ id: person.id, category: person.category, country: person.country, tax: person.tax, quantity: person.quantity, price: person.price, finalPrice: person.finalPrice }));
	//document.getElementById("demo").innerHTML = JSON.stringify(result);

let table = document.querySelector("table");
let data = Object.keys(result[0]);
generateTable(table, result);
generateTableHead(table, data);
document.getElementById("demo").innerHTML = JSON.stringify(result);

function calFunction()
{
	var tax = parseInt(document.getElementById("tax").value);
	var quant = parseInt(document.getElementById("quant").value);
	var price = parseInt(document.getElementById("price").value);
	var finalPrice = (price + (tax/100)*price)*quant;
	console.log(finalPrice);
    document.getElementById("final").innerHTML = JSON.stringify(finalPrice);
}